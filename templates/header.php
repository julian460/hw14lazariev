<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="E-shop with some gadgets">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <title>E-shop</title>
</head>
<body>
<div class="container">
    <div class="raw">
        <div class="col">
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link" href="/">Homepage</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/gadgets/create.php">Create new gadget</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/create_db.php">Create table</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/seader_db.php">Add gadgets to database</a>
                </li>
            </ul>
        </div>
    </div>
</div>