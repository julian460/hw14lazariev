<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/database/connect.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/Gadget.php';

$gadgets = [
    ['title' => 'phone', 'price' => 1000, 'd_escription' => 'some descripton here', 'type' => 'phone'],
    ['title' => 'laptop', 'price' => 2000, 'd_escription' => 'some descripton here', 'type' => 'laptop'],
    ['title' => 'watch', 'price' => 500, 'd_escription' => 'some descripton here', 'type' => 'watch']
];
foreach ($gadgets as $value) {
    $gadget = new Gadget(null, $value['title'], $value['price'], $value['d_escription'], $value['type']);
    $gadget->save($db);
}

echo 'Data added successfully';