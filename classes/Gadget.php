<?php
    class Gadget{
        public $id;
        public $title;
        public $price;
        public $d_escription;
        public $type;
    
        public function __construct($id = NULL, $title, $price, $d_escription, $type)
        {
            $this->id = $id;
            $this->title = $title;
            $this->price = $price;
            $this->d_escription = $d_escription;
            $this->type = $type;
        }
    
        public function save(PDO $db)
        {
            try {
                $sql = "INSERT INTO gadgets SET
                    title = :title,
                    price = :price,
                    d_escription = :d_escription,
                    type = :type;
                ";
                $statement = $db->prepare($sql);
                $statement->bindValue(':title', $this->title);
                $statement->bindValue(':price', $this->price);
                $statement->bindValue(':d_escription', $this->d_escription);
                $statement->bindValue(':type', $this->type);
                $statement->execute();
            } catch (Exception $e) {
                die('Problem with saving entry<br>' . $e->getMessage());
            }
        }
        public function update(PDO $db)
    {
        try {
            $sql = "UPDATE gadgets SET 
                title = :title,
                price = :price,
                d_escription = :d_escription,
                type = :type
                WHERE id=:id;
            ";
            $statement = $db->prepare($sql);
            $statement->bindValue(':title', $this->title);
            $statement->bindValue(':price', $this->price);
            $statement->bindValue(':d_escription', $this->d_escription);
            $statement->bindValue(':type', $type->type);
            $statement->bindValue(':id', $this->id);
            $statement->execute();
        } catch (Exception $e) {
            die('Error updating entry.' . $e->getMessage());
        }
    }    
    }    
    