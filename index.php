<?php 
require_once $_SERVER['DOCUMENT_ROOT'] . '/database/connect.php';
try {
    $sql = "SELECT * FROM gadgets;";
    $statement = $db->query($sql);
    $gadgets = $statement->fetchAll();
} catch (Exception $e) {
    die('Problem with getting data<br>' . $e->getMessage());
}
$message = NULL;
if (!empty($_GET['message'])) {
    $message = $_GET['message'];
}
?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/header.php'; ?>
    <div class="container">
        <div class="raw">
            <?php if ($message) : ?>
                <?php if ($message) : ?>
                    <?php switch ($message):
                        case 'created': ?>
                            <div class="alert alert-success">
                                Entry created successfully!
                            </div>
                            <?php break; ?>
                        <?php
                        case 'entry_updated': ?>
                            <div class="alert alert-primary">
                                Entry updated successfully!
                            </div>
                            <?php break; ?>
                    <?php endswitch; ?>
                <?php endif ?>
            <?php endif ?>
        </div>
           
            <?php foreach ($gadgets as $gadget) : ?>
                <div class="raw">
                <div class="col-4 card">
                    <div class="card-body">
                        <h5 class="card-title"><?= $gadget['title'] ?></h5>
                        <h6 class="card-subtitle mb-2 text-muted"><?= $gadget['type'] ?></h6>
                        <a href="/gadgets/show.php?id=<?= $gadget['id'] ?>" class="btn btn-primary">Read more</a>
                        <br>
                        <a href="/gadgets/edit.php?id=<?= $gadget['id'] ?>" class="btn btn-warning">Edit</a>
                        <br>
                        <form action="/gadgets/delete.php?id=<?= $gadget['id'] ?>" method="post"><button class="btn btn-danger">Delete<button></form>
                    </div>
                </div>
                </div>
            <?php endforeach; ?>
        
    </div>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/footer.php';?>