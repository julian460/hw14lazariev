<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/database/connect.php';

try{
    $sql = "CREATE TABLE gadgets (
        id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        title VARCHAR(255),
        price INT,
        d_escription TEXT,
        `type` VARCHAR(255)) 
        DEFAULT CHARACTER SET utf8 ENGINE=InnoDB;";
    $db->exec($sql);
}catch(Exception $e){
    die('Error creating gadgets table<br>' . $e->getMessage());
}