<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/database/connect.php';
try {
    $id = (int) $_GET['id'];
    $sql = "SELECT * FROM gadgets WHERE id=:id";
    $statement = $db->prepare($sql);
    $statement->bindValue(':id', $id);
    $statement->execute();
    $data = $statement->fetchAll();
} catch (Exception $e) {
    die('Error getting gadget.<br>' . $e->getMessage());
}
if (empty($data)) {
    header('Location:/');
}
$gadget = $data[0];
?>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/header.php'; ?>
<div class="container">
    <h1><?= $gadget['title'] ?></h1>
    <p><?= $gadget['type'] ?></p>
    <p><?= $gadget['price'] ?></p>
    <div class="row">
        <p>
            <?= $gadget['d_escription'] ?>
        </p>
    </div>
</div>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/footer.php'; ?>