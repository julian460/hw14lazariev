<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/database/connect.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/Gadget.php';

$id = (int) $_POST['id'];
$title = htmlspecialchars($_POST['title'], ENT_QUOTES, 'UTF-8');
$price = htmlspecialchars($_POST['price'], ENT_QUOTES, 'UTF-8');
$d_escription = htmlspecialchars($_POST['d_escription'], ENT_QUOTES, 'UTF-8');
$ty_pe = htmlspecialchars($_POST['type'], ENT_QUOTES, 'UTF-8');


$gadget = new Gadget($id, $title, $price, $d_escription, $type);
$gadget->update($db);

header('Location:/?message=entry_updated');
