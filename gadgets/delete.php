<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/database/connect.php';
$id = (int) $_GET['id'];
$sql = "DELETE FROM gadgets WHERE id=:id";
try {
    $statement = $db->prepare($sql);
    $statement->bindValue(':id', $id);
    $statement->execute();
} catch (Exception $e) {
    die('Error deleting gadgets.<br>' . $e->getMessage());
}
header('Location:/');
die();


