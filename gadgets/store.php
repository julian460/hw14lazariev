<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/database/connect.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/Gadget.php';
$title = htmlspecialchars($_POST['title'], ENT_QUOTES, 'UTF-8');
$price = htmlspecialchars($_POST['price'], ENT_QUOTES, 'UTF-8');
$d_escription = htmlspecialchars($_POST['d_escription'], ENT_QUOTES, 'UTF-8');
$ty_pe = htmlspecialchars($_POST['xxx'], ENT_QUOTES, 'UTF-8');

$entry = new Gadget(null, $title, $price, $d_escription, $xxx);
$entry->save($db);

header('Location:/?message=created');
