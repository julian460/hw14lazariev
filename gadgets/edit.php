<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/database/connect.php';
try {
    $id = (int) $_GET['id'];
    $sql = "SELECT * FROM gadgets WHERE id=:id";
    $statement = $db->prepare($sql);
    $statement->bindValue(':id', $id);
    $statement->execute();
    $data = $statement->fetchAll();
} catch (Exception $e) {
    die('Error getting entry.<br>' . $e->getMessage());
}
if (empty($data)) {
    header('Location:/');
}
$gadget = $data[0];
?>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/header.php'; ?>
<div class="container">
    <h1>Edit entry:</h1>
    <div class="row d-flex justify-content-center">
        <div class="col-8">
            <form method="post" action="/gadgets/update.php">
                <input type="hidden" name="id" value="<?= $id ?>">
                <div class="mb-3">
                    <label for="title" class="form-label">Entry title:</label>
                    <input name='title' type="text" class="form-control" id="title" value="<?= $gadget['title'] ?>">
                </div>
                <div class="mb-3">
                    <label for="title" class="form-label">Entry title:</label>
                    <input name='price' type="number" class="form-control" id="price" value="<?= $gadget['price'] ?>">
                </div>
                <div class="mb-3">
                    <select name='type' class="form-select" aria-label="Type of gadget">
                        <option selected>Open it</option>
                        <option value="watch">Watch</option>
                        <option value="phone">Phone</option>
                        <option value="laptop">Laptop</option>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="d_escription" class="form-label">Entry text:</label>
                    <textarea name='d_escription' class="form-control" id="d_escription"><?= $gadget['d_escription'] ?></textarea>
                </div>
                <div>
                    <button class="btn btn-primary">Edit entry</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/footer.php'; ?>