<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/header.php';?> 
<div class="container">
    <h1>Create new gadget:</h1>
    <div class="row d-flex justify-content-center">
       <div class="col-8">
            <form method="post" action="/gadgets/store.php">
                <div class="mb-3">
                    <label for="title" class="form-label">Entry title:</label>
                    <input name='title' type="text" class="form-control" id="title">
                </div>
                <div class="mb-3">
                    <label for="d_escription" class="form-label">Entry description:</label>
                    <textarea name='d_escription' class="form-control" id="d_escription"></textarea>
                </div>
                <div class="mb-3">
                    <label for="price" class="form-label">Entry price:</label>
                    <input name='price' type="number" class="form-control" id="price">
                </div>
                <div class="mb-3">
                    <select name='type' class="form-select" aria-label="Type of gadget">
                        <option selected>Open it</option>
                        <option value="watch">Watch</option>
                        <option value="phone">Phone</option>
                        <option value="laptop">Laptop</option>
                    </select>
                </div>
                <div>
                    <button class="btn btn-primary">Create entry</button>
                </div>
            </form>
       </div>
    </div>
</div>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/footer.php';?>